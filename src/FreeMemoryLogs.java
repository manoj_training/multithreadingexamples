import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FreeMemoryLogs {
	static int counter=1;
	public static void main(String[] args) throws Exception {

		Runtime runtime=Runtime.getRuntime();
		
		ScheduledExecutorService service=Executors.newSingleThreadScheduledExecutor();
		System.out.println("Service Started.....!");
		service.scheduleAtFixedRate(()->{
			System.gc();
			Date dt=new Date();
			long free=runtime.freeMemory();
			String msg="";
			try {
				//FileWriter writer=new FileWriter("d:/data/memoryinfo.txt",true);
				FileOutputStream stream=new FileOutputStream("d:/data/memoryinfo.txt",true);
				msg="Free Memory On "+dt+" : "+free+"\n";
			    System.out.println(msg);
				stream.write(msg.getBytes());
				counter++;
				if(counter==6) {
					stream.close();
					//service.shutdown();
					System.out.println("Servie Stoped...!");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}, 5, 10, TimeUnit.SECONDS);
	}
}
