import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class SingleThreadedReading {

	public static void main(String[] args) throws Exception {

		String name = Thread.currentThread().getName();

		FileInputStream fis1 = new FileInputStream("d:/data/info1.txt");

		BufferedReader br1 = new BufferedReader(new InputStreamReader(fis1));
		while (true) {
			String line = br1.readLine();
			if (line == null)
				break;
			System.out.println(name + " => " + line);
		}
		br1.close();
		fis1.close();
		

		FileInputStream fis2 = new FileInputStream("d:/data/info2.txt");

		BufferedReader br2 = new BufferedReader(new InputStreamReader(fis2));
		while (true) {
			String line = br2.readLine();
			if (line == null)
				break;
			System.out.println(name + " => " + line);
		}
		br2.close();
		fis2.close();
	}

}
