import java.util.Scanner;

public class Emp extends Thread {
	private int eno;
	private int salary;
	public Emp(int eno, int salary) {this.eno=eno; this.salary=salary;}

	public void run() {
		processSalary();
	}
	
	public void processSalary() {
		System.out.println("salary of "+eno+" : "+salary);
		if(eno==112) {
			Scanner sc=new Scanner(System.in);
			sc.nextInt();
		}
		System.out.println("hra of "+eno+" : "+(salary*20/100));
		System.out.println("ca of "+eno+" : "+(salary*10/100));
		System.out.println("ma of "+eno+" : "+(salary*5/100));
		
	}
	
	public static void main(String args[]) {
		Emp e1=new Emp(111,10000);
		Emp e2=new Emp(112,20000);
		Emp e3=new Emp(113,30000);
		Emp e4=new Emp(114,40000);
		e1.start(); e2.start();
		e3.start(); e4.start();
		/*
		e1.processSalary();
		e2.processSalary();
		e3.processSalary();
		e4.processSalary();
		*/
		
	}
}
