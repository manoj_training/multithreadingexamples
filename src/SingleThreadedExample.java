
public class SingleThreadedExample {
	static int sum;
	
	public static void main(String[] args) throws Exception {
		
		long start=System.currentTimeMillis();
		
		Thread t1=new Thread(()->{
			for(int i=1;i<=50000;i++) {
				String name=Thread.currentThread().getName();
				System.out.println("Adding "+i+" To Sum By "+name);
				sum=sum+i;
			}
		}); t1.setName("Dharmendra"); t1.start();	
		
		t1.join();
		System.out.println("Sum "+sum);
		
		long stop=System.currentTimeMillis();
		
		System.out.println("Started at : "+start);
		System.out.println("Stoped at : "+stop);
		System.out.println("Time Taken : "+(stop-start));
	}

}
