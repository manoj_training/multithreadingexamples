import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class MultiThreadedReading {

	public static void main(String[] args) throws Exception {
		System.out.println("Main Thread Started....!");
		Thread t1=new Thread(()->{
			//task-1
			try {
			String name = Thread.currentThread().getName();
			FileInputStream fis = new FileInputStream("d:/data/info1.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			while (true) {
				String line = br.readLine();
				if (line == null)
					break;
				System.out.println(name + " => " + line);
			}
			br.close();
			fis.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		});
		t1.setName("ReaderOneThread ");
		t1.start();
		
		
		Thread t2=new Thread(()->{
			int cnt=0;
			//task-2
			try {
				String name = Thread.currentThread().getName();
				FileInputStream fis = new FileInputStream("d:/data/info2.txt");
				BufferedReader br = new BufferedReader(new InputStreamReader(fis));
				while (true) {
					String line = br.readLine();
					if (line == null)
						break;
					System.out.println(name + " => " + line);
					//Thread.sleep(100);
					/*
					cnt++;
					if(cnt==4) {
						//System.out.println(10/0);
						//Thread.currentThread().stop();
						Thread.sleep(25000); //cause the running thread to sleep for given milliseconds.
					}
					*/
				}
				br.close();
				fis.close();
				}catch(Exception e) {
					e.printStackTrace();
				}
		});
		
		t2.setName("ReaderTwoThread ");
		t2.start();
		//here I want main thread to be blocked till the t1 and t2 thread goes to dead state
		
		t1.join();//cause running thread (main) to blocked till the invoking thread (t1) goes to dead state
		t2.join();//cause running thread (main) to blocked till the invoking thread (t2) goes to dead state.
		System.out.println("Reading For Both Files Completed.........!!!!!!");
		System.out.println("END OF MAIN.............................!!!!!!!!");

	}

}
