


public class MultiThreadingExampleThree {

	public static void main(String[] args) {
		
		Thread t1=new Thread(new Runnable() {

			@Override
			public void run() {
				String name=Thread.currentThread().getName();
				for(int i=1; i<=10;i++) {
					System.out.println(name+"=>2 x "+i+" = "+2*i);
				}
				
			}
			
		});
		Thread t2=new Thread(new Runnable() {

			@Override
			public void run() {
				String name=Thread.currentThread().getName();
				for(int i=1; i<=10;i++) {
					System.out.println(name+"=>3 x "+i+" = "+3*i);
				}
				
			}
			
		});
		Thread t3=new Thread(new Runnable() {

			@Override
			public void run() {
				String name=Thread.currentThread().getName();
				for(int i=1; i<=10;i++) {
					System.out.println(name+"=>4 x "+i+" = "+4*i);
				}
				
			}
			
		});
		t1.setName("First ");		t2.setName("Second ");		t3.setName("Third ");;
		t1.start(); t2.start(); t3.start();
	}

}
