package executor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduleExecutorExampleTwo {
	static int counter=1;
	public static void main(String[] args) {
		
		ScheduledExecutorService service=Executors.newSingleThreadScheduledExecutor();
		System.out.println("Submitting Task ....!"+new java.util.Date());
		
		service.scheduleAtFixedRate(()->{
			System.out.println("Counter : "+counter+" Task Started ....!"+new java.util.Date());
			for(int i=1;i<=5;i++) {
				System.out.println(i);
			}
			counter++;
			if(counter==5) {
				service.shutdown();
			}
			
		}, 30, 15, TimeUnit.SECONDS);
		
		
		//service.shutdown();

	}

}
