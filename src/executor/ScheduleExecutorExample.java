package executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduleExecutorExample {

	public static void main(String[] args) {
		
		ScheduledExecutorService service=Executors.newSingleThreadScheduledExecutor();
		System.out.println("Submitting Task ....!"+new java.util.Date());
		service.schedule(()->{
			System.out.println("Task Started ....!"+new java.util.Date());
			for(int i=1;i<=5;i++) {
				System.out.println(i);
			}
		}, 30, TimeUnit.SECONDS);
		service.shutdown();
		
	}

}
