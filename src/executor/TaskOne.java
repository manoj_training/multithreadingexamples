package executor;

import java.util.HashSet;
import java.util.Scanner;

public class TaskOne {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		HashSet<String> names=new HashSet<String>();
		
		
		while(true) {
		
			System.out.println("Enter Name : ");
			String name=sc.next();
			boolean success=names.add(name);
			if(success==false) {
				System.out.println(name+" is already avaialble...!");
			}
			System.out.println("Total Names In Set  : "+names.size());
			if(names.size()==10) {
				break;
			
			}
		}
		System.out.println("--------------------------------------------------------");
		names.forEach((name)->System.out.println(name));

	}

}
