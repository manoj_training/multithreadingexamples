package executor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduleExecutorExampleThree {

	public static void main(String[] args) {
		ScheduledExecutorService service=Executors.newSingleThreadScheduledExecutor();
		System.out.println("Submitting Task ....!"+new java.util.Date());
		
		//service.scheduleAtFixedRate(()->{
		service.scheduleWithFixedDelay(()->{
			System.out.println("Task Started ....!"+new java.util.Date());
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Task End ........!"+new java.util.Date());
			System.out.println("__________________________________________________");
		}, 30, 30, TimeUnit.SECONDS);
		
		//service.shutdown();

	}

}
