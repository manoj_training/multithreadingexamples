package executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleThreadExecutorExample {

	public static void main(String[] args) {
		
		ExecutorService service = Executors.newSingleThreadExecutor();

		service.submit(() -> {
			System.out.println("Task - 1 Completed By : " + Thread.currentThread().getName());
		});

		service.submit(() -> {
			System.out.println("Task - 2 Completed By : " + Thread.currentThread().getName());
		});

		service.submit(() -> {
			System.out.println("Task - 3 Completed By : " + Thread.currentThread().getName());
		});

		service.submit(() -> {
			System.out.println("Task - 4 Completed By : " + Thread.currentThread().getName());
		});
		service.submit(() -> {
			System.out.println("Task - 5 Completed By : " + Thread.currentThread().getName());
		});
		service.submit(() -> {
			System.out.println("Task - 6 Completed By : " + Thread.currentThread().getName());
		});

		// wait for a new task to submit

		service.shutdown();

	}

}
