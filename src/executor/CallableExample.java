package executor;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableExample {

	public static void main(String[] args) throws Exception {
		
		ExecutorService service=Executors.newSingleThreadExecutor();
		
		Callable<Integer> task=new Callable<Integer>() {
			@Override
			public Integer call() throws Exception {
				int sum=0;
				for(int i=1; i<=10; i++) {
					sum=sum+i;
				}
				Thread.sleep(25000);
				return sum;
			}
		};
		System.out.println("Submitted The Task ...!");
		Future<Integer> future=service.submit(task);
	
		System.out.println("doing other job1..");
		System.out.println("doing other job2..");
		System.out.println("doing other job3..");
		
		int result=future.get();
		System.out.println("Result : "+result);
		
	}

}
