package executor;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableExampleTwo {

	public static void main(String[] args) throws Exception {
		
		//task-1 (1 to 10)
		//task-2 (11 to 20)
		//task-3 (21 to 30)
		
		Callable<Integer> task1=()->{
			int sum=0;
			for(int i=1;i<=10;i++) {
				sum=sum+i;
			}
			return sum;
		};

		Callable<Integer> task2=()->{
			int sum=0;
			for(int i=11;i<=20;i++) {
				sum=sum+i;
			}
			return sum;
		};
		
		Callable<Integer> task3=()->{
			int sum=0;
			for(int i=21;i<=30;i++) {
				sum=sum+i;
			}
			return sum;
		};
		
		List<Callable<Integer>> callableList=Arrays.asList(task1,task2,task3);
		
		ExecutorService service=Executors.newFixedThreadPool(3);
		
		
		Integer result=service.invokeAny(callableList);
		System.out.println("Result : "+result);
		/*
		List<Future<Integer>> futures=service.invokeAll(callableList);
		
		
		for(Future<Integer> future:futures) {
			System.out.println(future.get());
		}
		*/
		
	}

}
