
public class MultiThreadedExample {

	static int s1,s2,s3,sum;
	
	public static void main(String[] args) throws Exception {
		long start=System.currentTimeMillis();
		// adding 1 to 50000 using multiple threads

		Thread t1=new Thread(()->{
			for(int i=1;i<=17000;i++) {
				String name=Thread.currentThread().getName();
				//System.out.println("Adding "+i+" To Sum By "+name);
				s1=s1+i;
			}
		}); t1.setName("Abhishek"); t1.start();
		
		Thread t2=new Thread(()->{
			for(int i=17001;i<=34000;i++) {
				String name=Thread.currentThread().getName();
				//System.out.println("Adding "+i+" To Sum By "+name);
				s2=s2+i;
			}
		}); t2.setName("Sumit"); t2.start();
		
		Thread t3=new Thread(()->{
			for(int i=34001;i<=50000;i++) {
				String name=Thread.currentThread().getName();
				//System.out.println("Adding "+i+" To Sum By "+name);
				s3=s3+i;
			}
		}); t3.setName("Manas"); t3.start();
		
		t1.join(); t2.join(); t3.join();
		System.out.println("Addition By Abhishek : "+s1);
		System.out.println("Addition By Sumit    : "+s2);
		System.out.println("Addition By Manas    : "+s3);
		sum=s1+s2+s3;
		System.out.println("Addition : "+sum);
		
		long stop=System.currentTimeMillis();
		System.out.println("Started at : "+start);
		System.out.println("Stoped at : "+stop);
		System.out.println("Time Taken : "+(stop-start));
	}

}
