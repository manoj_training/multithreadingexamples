
public class BankingApplication {

	public static void main(String[] args) {
		
		Account ac1=new Account(111,10000);
		Account ac2=new Account(112,20000);
		Account ac3=new Account(113,30000);
		//ac1.withdraw(5000);
		//ac2.withdraw(8000);
		//ac3.withdraw(11000);
		
		Thread t1=new Thread(()->{ac1.withdraw(5000);}); t1.setName("Abhishek");
		Thread t2=new Thread(()->ac2.withdraw(8000)); t2.setName("Sumit");
		Thread t3=new Thread(()->ac1.withdraw(9000)); t3.setName("Manas");
		
		t1.start();
		t2.start();
		t3.start();
	}

}
