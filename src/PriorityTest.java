
public class PriorityTest {
	public static void main(String[] args) throws Exception {
		
		Thread t1=new Thread(()->{
			Thread tmp=Thread.currentThread();
			System.out.println(tmp.getName());
		});
		t1.start();
		
		Thread t2=new Thread(()->{
			Thread tmp=Thread.currentThread();
			System.out.println(tmp.getName());
		});
		t2.start();
		t1.setPriority(Thread.MAX_PRIORITY);
		t2.setPriority(Thread.MIN_PRIORITY);
		System.out.println(t1.getPriority());
		System.out.println(t2.getPriority());
		
		Thread.sleep(5000);
		
		
		
		Thread tmp=Thread.currentThread();
		System.out.println(tmp.getPriority());
		System.out.println(tmp.getName());
	}
}
